module.exports = {
	siteMetadata: {
		title: 'test pwa',
	},
	plugins: [
		'gatsby-plugin-react-helmet',
		'gatsby-plugin-offline',
		{
			resolve: 'gatsby-plugin-manifest',
			options: {
				icon: 'src/images/icon.png',
				name: 'test pwa',
				short_name: 'test pwa',
				description: 'test pwa',
				lang: 'en',
				start_url: '.',
				display: 'standalone',
			},
		},
		{
			resolve: 'gatsby-plugin-create-client-paths',
			options: { prefixes: ['/*'] },
		},
	],
};
