import * as React from 'react';
import { Router } from '@reach/router';
import LoginPage from '../components/pages/LoginPage';
import AppPage from '../components/pages/AppPage';

const IndexPage = () => {
	return (
		<Router>
			<LoginPage path="/" />
			<AppPage path="/app" />
		</Router>
	);
};

export default IndexPage;
