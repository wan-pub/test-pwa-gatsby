import { navigate } from '@reach/router';
import React from 'react';

const onClick = () => {
	navigate('/app');
};

export default function LoginPage() {
	return (
		<div>
			<div> login page</div>
			<button type="button" onClick={onClick}>
				login
			</button>
		</div>
	);
}
