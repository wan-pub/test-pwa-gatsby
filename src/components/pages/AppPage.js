import { navigate } from '@reach/router';
import React from 'react';

const onClick = () => {
	navigate('/');
};

export default function LoginPage() {
	return (
		<div>
			<div>app page</div>
			<button type="button" onClick={onClick}>
				logout
			</button>
		</div>
	);
}
