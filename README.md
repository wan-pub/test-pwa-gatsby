# Reproducing `gatsby-plugin-offline` + `gatsby-plugin-create-client-paths` = blank page :(

1. Install npm packages

```sh
npm i
```

1. Install the SSL cert generator: [https://github.com/FiloSottile/mkcert](https://github.com/FiloSottile/mkcert)

1. Generate a valid SSL cert for your local IP address, e.g. `mkcert 192.168.1.100`

1. Build and serve the site

```sh
npm run secure:serve -- --ssl-cert "<cert file>" --ssl-key "<key file>"
```
